# Project Title

[![pipeline status](https://gitlab.com/serrodcal-ci-poc/hello-buildingblock/badges/master/pipeline.svg)](https://gitlab.com/serrodcal-ci-poc/hello-buildingblock/commits/master)

Microservice orchestrator example.

## Getting Started

### Prerequisites

Run previous project with other microservices needed: 

* [Formatter](https://gitlab.com/serrodcal-ci-poc/formatter-buildingblock)
* [Publisher](https://gitlab.com/serrodcal-ci-poc/publisher-buildingblock)

Install Maven in your local and Java 8. After that, clone this repo.

### Installing

Compile `~$ mvn clean package` and run:

```
java -jar java-docker-opentracing-poc-hello-1.0-SNAPSHOT.jar server server.yml
```

Console must show:

```
...
INFO  [2018-03-05 06:00:26,630] io.dropwizard.jersey.DropwizardResourceConfig: The following paths were found for the configured resources:

    GET     /hello (Hello.HelloResource)

INFO  [2018-03-05 06:00:26,632] org.eclipse.jetty.server.handler.ContextHandler: Started i.d.j.MutableServletContextHandler@343fddd9{/,null,AVAILABLE}
INFO  [2018-03-05 06:00:26,637] io.dropwizard.setup.AdminEnvironment: tasks = 

    POST    /tasks/log-level (io.dropwizard.servlets.tasks.LogConfigurationTask)
    POST    /tasks/gc (io.dropwizard.servlets.tasks.GarbageCollectionTask)

WARN  [2018-03-05 06:00:26,637] io.dropwizard.setup.AdminEnvironment: 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    THIS APPLICATION HAS NO HEALTHCHECKS. THIS MEANS YOU WILL NEVER KNOW      !
!     IF IT DIES IN PRODUCTION, WHICH MEANS YOU WILL NEVER KNOW IF YOU'RE      !
!    LETTING YOUR USERS DOWN. YOU SHOULD ADD A HEALTHCHECK FOR EACH OF YOUR    !
!         APPLICATION'S DEPENDENCIES WHICH FULLY (BUT LIGHTLY) TESTS IT.       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
INFO  [2018-03-05 06:00:26,644] org.eclipse.jetty.server.handler.ContextHandler: Started i.d.j.MutableServletContextHandler@468173fa{/,null,AVAILABLE}
INFO  [2018-03-05 06:00:26,655] org.eclipse.jetty.server.AbstractConnector: Started application@45404d5{HTTP/1.1,[http/1.1]}{0.0.0.0:8080}
INFO  [2018-03-05 06:00:26,656] org.eclipse.jetty.server.AbstractConnector: Started admin@29138d3a{HTTP/1.1,[http/1.1]}{0.0.0.0:8085}
INFO  [2018-03-05 06:00:26,656] org.eclipse.jetty.server.Server: Started @2121ms
```

## Running the tests

Run `~$ curl 'http://localhost:8080/hello?helloTo=Sergio&greeting=Hola'` to test application.

## Deployment

To deploy is able to use Docker as given below:

```
~$ docker pull registry.gitlab.com/serrodcal-ci-poc/hello-buildingblock:latest
~$ docker run -p8080:8080 -d <Image_ID>
```

## Built With

* [Dropwizard](http://www.dropwizard.io/1.2.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Gitlab CI](https://docs.gitlab.com/ee/ci/) -  Gitlab CI

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sergio Rodríguez Calvo** - *Development* - [serrodcal](https://github.com/serrodcal)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
